#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const FS = require("fs");
const HTTP = require("http");
const Liquid = require("liquid");
const NodeStatic = require("node-static");
const Path = require("path");
const RMRF = require("rimraf");
const YAML = require("js-yaml");
const ConfigHelper_1 = require("./src/ConfigHelper");
const Content_1 = require("./src/Content");
const JekyllJSHighlight_1 = require("./src/JekyllJSHighlight");
const YAMLConfig_1 = require("./src/YAMLConfig");
const Site_1 = require("./src/Site");
const LiquidHighlight = require("./src/LiquidHighlight");
const IncludeIfExists_1 = require("./src/IncludeIfExists");
const marked_1 = require("marked");
const highlighter = new JekyllJSHighlight_1.JekyllJSHighlight();
const liquidEngine = new Liquid.Engine();
const config = new ConfigHelper_1.ConfigHelper();
const yamlConfig = new YAMLConfig_1.YAMLConfig();
const context = {
    site: new Site_1.Site(),
};
const layouts = {};
function run() {
    const startTime = new Date();
    const configJSONPath = (process.argv.length > 2) ? process.argv[2] : '';
    if (!configJSONPath) {
        console.error(`No config json path was passed - aborting`);
        process.exit(1);
    }
    _initConfig(configJSONPath);
    _createLog();
    _readYAMLConfig();
    _initContext();
    _initJekyllJSHighlight();
    _createMarked();
    _initLiquidEngine();
    const isServing = (process.argv.length > 3 && process.argv[3] === "serve");
    if (isServing) {
        context.site.url = "http://localhost:" + config.server.port;
        console.debug(`We're serving the site; changing the site url to ${context.site.url}`);
    }
    console.debug("Reading content");
    _readLayouts();
    _readContents("_posts", context.site.posts);
    _readContents("pages", context.site.pages);
    const numTags = Object.keys(context.site.tags).length;
    context.site.tags.size = numTags;
    console.info(`${Object.keys(layouts).length} layouts, ${context.site.posts.length} posts, and ${context.site.pages.length} pages were read. A total of ${numTags} tags were found`);
    console.debug("Parsing liquid tags in our content");
    _convertPostsAndPages().then(function () {
        var destDir = Path.join(config.src.path, yamlConfig.destination);
        _rmrfDir(destDir);
        FS.mkdirSync(destDir);
        return destDir;
    }).then(function (destDir) {
        console.info("Saving site content");
        return _savePostsAndPages(destDir);
    }).then(function () {
        console.info("Saving other site files");
        return _copyAllOtherFiles(config.src.path);
    }).then(function () {
        const totalTimeMS = ((new Date()).getTime() - startTime.getTime());
        let totalTimeS = Math.floor(totalTimeMS / 1000);
        let msg = "JekyllJS build finished in";
        let mins = 0;
        if (totalTimeS >= 60) {
            mins = Math.floor(totalTimeS / 60);
            totalTimeS -= 60 * mins;
            msg += (mins == 1) ? " 1 min" : " " + mins + " mins";
        }
        if (totalTimeS > 0)
            msg += (totalTimeS == 1) ? " 1 second!" : " " + totalTimeS + " seconds!";
        else if (mins == 0)
            msg += " no time at all!";
        console.info(msg);
        if (isServing) {
            console.info("Starting local server");
            _createServer();
            return;
        }
        process.exit();
    }).catch(function (e) {
        console.error("Aborting build because an error occurred", e);
        process.exit();
        return;
    });
}
run();
function _createLog() {
    console.info(`JekyllJS starting up on ${new Date()}`);
    console.debug("Config", config);
}
function _initConfig(path) {
    try {
        const json = FS.readFileSync(path, 'utf-8');
        const jsonObj = JSON.parse(json);
        config.parse(jsonObj, path);
    }
    catch (e) {
        console.error("An error occurred when loading our config", e);
        process.exit(1);
    }
}
function _readYAMLConfig() {
    const path = Path.join(config.src.path, "_config.yml");
    console.debug(`Reading yaml config from ${path}`);
    let yaml = {};
    if (FS.existsSync(path))
        yaml = YAML.load(FS.readFileSync(path, "utf-8"));
    else
        console.warn(`The YAML config file (${path}) doesn't exist`);
    yamlConfig.fromObj(yaml);
    console.debug("YAML config:", yamlConfig);
}
function _initContext() {
    context.site.meta = config.meta;
    context.site.opengraph = config.opengraph;
    context.site.updateFromYAML(yamlConfig);
}
function _initJekyllJSHighlight() {
    highlighter.config = config.highlight;
}
function _createMarked() {
    const renderer = new marked_1.marked.Renderer();
    renderer.code = function (code, lang) {
        return highlighter.highlight(code, lang) + '\n';
    };
    marked_1.marked.setOptions({
        renderer: renderer
    });
}
function _initLiquidEngine() {
    const escapeEntityMap = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        '"': "&quot;",
        "¢": "&cent;",
        "£": "&pound;",
        "¥": "&yen;",
        "€": "&euro;",
        "©": "&copy;",
        "®": "&reg;"
    };
    let escapeREStr = "[";
    for (let char in escapeEntityMap)
        escapeREStr += char;
    escapeREStr += "]";
    const escapeRE = new RegExp(escapeREStr, "g");
    function escapeReplace(matchedEntity) {
        return escapeEntityMap[matchedEntity];
    }
    liquidEngine.registerFilters({
        "date_to_xmlschema": function (date) {
            var d = (typeof date === 'string') ? new Date(date) : date;
            return d.toISOString();
        },
        "xml_escape": function (input) {
            return input.replace(escapeRE, escapeReplace);
        },
        "cgi_escape": function (input) {
            return encodeURIComponent(input).replace(/%20/g, "+");
        },
        "base64": function (input) {
            return Buffer.from(input, 'utf-8').toString('base64');
        }
    });
    LiquidHighlight.highlighter = highlighter;
    liquidEngine.registerTag("highlight", LiquidHighlight);
    liquidEngine.registerTag("include_if_exists", IncludeIfExists_1.IncludeIfExists);
    const includePath = Path.join(config.src.path, yamlConfig.includes_dir);
    const lfs = new Liquid.LocalFileSystem(includePath, "html");
    liquidEngine.registerFileSystem(lfs);
}
function _readLayouts() {
    const path = Path.join(config.src.path, yamlConfig.layouts_dir);
    if (!FS.existsSync(path)) {
        console.warn(`Can't read any layouts as the dir '${path}' doesn't exist`);
        return;
    }
    FS.readdirSync(path).forEach(function (filename) {
        if (!_isSupportedContentType(filename)) {
            console.info(`Ignoring the file '${filename}' (${path}) in layouts as it's not a supported content type (HTML/Markdown)`);
            return;
        }
        const contentsRaw = FS.readFileSync(Path.join(path, filename), 'utf-8');
        const layoutName = filename.substring(0, filename.lastIndexOf("."));
        layouts[layoutName] = contentsRaw;
    });
}
function _readContents(dir, ar) {
    const path = Path.join(config.src.path, dir);
    if (!FS.existsSync(path)) {
        console.warn(`The dir '${path}' doesn't exist`);
        return;
    }
    FS.readdirSync(path).forEach(function (filename) {
        if (!_isSupportedContentType(filename)) {
            console.info(`Ignoring the file '${filename}' (${path}) as it's not a supported content type (HTML/Markdown)`);
            return;
        }
        const content = _readContent(path, filename);
        if (content) {
            _extractTags(content);
            ar.push(content);
        }
    });
    ar.sort(_sortContent);
}
function _readContent(path, filename) {
    const filePath = Path.join(path, filename);
    if (!FS.existsSync(filePath)) {
        console.warn(`Can't read the content '${filePath}' as the file doesn't exist`);
        return undefined;
    }
    const contentsRaw = FS.readFileSync(filePath, 'utf-8');
    const content = new Content_1.Content(Path.relative(config.src.path, filePath));
    content.readFromFile(filename, contentsRaw);
    return content;
}
function _extractTags(content) {
    if (content.tags == null || content.tags.length == 0)
        return;
    const len = content.tags.length;
    for (var i = 0; i < len; i++) {
        const t = content.tags[i];
        if (t in context.site.tags) {
            context.site.tags[t].push(content);
            context.site.tags[t].sort(_sortContent);
        }
        else
            context.site.tags[t] = [content];
    }
}
function _convertPostsAndPages() {
    let sequence = Promise.resolve();
    context.site.posts.forEach(function (post) {
        sequence = sequence.then(function () {
            return _convertContent(post);
        });
    });
    context.site.pages.forEach(function (page) {
        sequence = sequence.then(function () {
            return _convertContent(page);
        });
    });
    return sequence;
}
function _convertContent(content) {
    context.page = content;
    context.content = content.content;
    return liquidEngine.parseAndRender(content.content, context).then(function (result) {
        console.debug(`Finished parsing liquid in ${content.filename}`);
        content.content = result;
        if (content.isMarkdown) {
            console.debug(`Converting markdown file '${content.filename}'`);
            content.content = (0, marked_1.marked)(content.content);
        }
    }).catch(function (e) {
        console.error(`Couldn't parse liquid in ${content.filename}`, e);
        throw e;
    });
}
function _rmrfDir(dir) {
    if (FS.existsSync(dir) && FS.statSync(dir).isDirectory()) {
        console.debug(`Clearing dir ${dir}`);
        RMRF.sync(dir);
    }
}
function _savePostsAndPages(destRoot) {
    let sequence = Promise.resolve();
    context.site.posts.forEach(function (post) {
        sequence = sequence.then(function () {
            _ensureDirs(post.url, destRoot);
            return _saveContent(post, post.path, Path.join(destRoot, post.url, "index.html"));
        });
    });
    context.site.pages.forEach(function (page) {
        sequence = sequence.then(function () {
            _ensureDirs(page.url, destRoot);
            return _saveContent(page, page.path, Path.join(destRoot, page.url, "index.html"));
        });
    });
    return sequence;
}
function _saveContent(content, path, destPath) {
    if (content == null)
        return Promise.reject(new Error(`Can't save some content in path ${destPath} as null was passed`));
    if (content.frontMatter) {
        const layout = (content.layout) ? layouts[content.layout] : undefined;
        if (layout) {
            context.page = content;
            context.content = content.content;
            return liquidEngine.parseAndRender(layout, context).then(function (result) {
                console.debug(`Saving file ${content.url}`);
                FS.writeFileSync(destPath, result, 'utf-8');
            }).catch(function (e) {
                console.error(`Couldn't save content ${content.url}`, e);
                throw e;
            });
        }
        else {
            return _convertContent(content).then(function () {
                console.debug(`Saving file ${content.url}`);
                FS.writeFileSync(destPath, content.content, 'utf-8');
            }).catch(function (e) {
                console.error(`Couldn't save content ${content.url}`, e);
                throw e;
            });
        }
    }
    else {
        console.debug(`Saving file ${content.url}`);
        _copyFile(path, destPath);
        return Promise.resolve();
    }
}
function _copyAllOtherFiles(dir, sequence) {
    if (!sequence)
        sequence = Promise.resolve();
    FS.readdirSync(dir).forEach(function (filename) {
        const path = Path.join(dir, filename);
        if (/^[_\.]/.test(filename)) {
            if (yamlConfig.include.indexOf(filename) == -1) {
                console.debug(`Ignoring ${path} as it's a hidden file, or a special directory`);
                return;
            }
            console.debug(`Including ${path} as it's in our YAML include array`);
        }
        if (yamlConfig.exclude.indexOf(filename) != -1) {
            console.debug(`Ignoring ${Path.join(dir, filename)} as it's in our YAML exclude array`);
            return;
        }
        if (FS.statSync(path).isDirectory()) {
            if (filename == "pages")
                return;
            _copyAllOtherFiles(path, sequence);
        }
        else {
            const destRootDir = Path.join(config.src.path, yamlConfig.destination);
            const destDir = Path.join(destRootDir, Path.relative(config.src.path, dir));
            const destPath = Path.join(destDir, filename);
            _ensureDirs(Path.relative(destRootDir, destDir), destRootDir);
            const content = _readContent(dir, filename);
            if (content) {
                if (content.url == "/index.html")
                    content.url = "/";
                if (content.frontMatter)
                    context.site.pages.push(content);
                sequence = sequence.then(function () {
                    return _saveContent(content, path, destPath);
                });
            }
        }
    });
    return sequence;
}
function _copyFile(inPath, outPath) {
    FS.copyFileSync(inPath, outPath);
}
function _ensureDirs(path, root) {
    let curr = `${root}${Path.sep}`;
    const parts = (path.indexOf("/") != -1) ? path.split("/") : path.split(Path.sep);
    const len = parts.length;
    for (var i = 0; i < len; i++) {
        if (i == len - 1 && parts[i].indexOf(".") != -1)
            return;
        curr += `${parts[i]}${Path.sep}`;
        curr = Path.normalize(curr);
        if (!FS.existsSync(curr))
            FS.mkdirSync(curr);
    }
}
function _sortContent(a, b) {
    return b.date.getTime() - a.date.getTime();
}
function _isSupportedContentType(filename) {
    const index = filename.lastIndexOf('.');
    if (index == -1)
        return false;
    const ext = filename.substring(index + 1);
    return (ext == 'html' || ext == 'htm' || ext == 'md' || ext == 'markdown');
}
function _createServer() {
    const serverDir = Path.join(config.src.path, yamlConfig.destination);
    const file = new NodeStatic.Server(serverDir, {
        cache: 0,
        gzip: true
    });
    if (config.src.fourOhFour) {
        const path404 = Path.join(serverDir, config.src.fourOhFour);
        const exists = FS.existsSync(path404);
        if (!exists) {
            console.error(`The 404 path specified (${path404}) doesn't exist`);
            config.src.fourOhFour = undefined;
        }
        else if (FS.statSync(path404).isDirectory()) {
            if (config.src.fourOhFour.charAt(config.src.fourOhFour.length - 1) == "/")
                config.src.fourOhFour += "index.html";
            else
                config.src.fourOhFour += "/index.html";
        }
        if (config.src.fourOhFour && config.src.fourOhFour.charAt(0) != "/")
            config.src.fourOhFour = `/${config.src.fourOhFour}`;
    }
    HTTP.createServer(function (request, response) {
        request.addListener('end', function () {
            file.serve(request, response, function (err) {
                if (err) {
                    console.error(`There was an error getting ${request.url} - ${err.message}`);
                    if (config.src.fourOhFour && request.url && (request.url.indexOf(".html") != -1 || request.url.charAt(request.url.length - 1) == "/") && err.status == 404)
                        file.serveFile(config.src.fourOhFour, 404, {}, request, response);
                    else {
                        response.writeHead(err.status, err.headers);
                        response.end();
                    }
                }
            });
        }).resume();
    }).listen(config.server.port);
    console.info(`Local server started at ${context.site.url}`);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUVBLHlCQUEwQjtBQUMxQiw2QkFBOEI7QUFDOUIsaUNBQWtDO0FBQ2xDLDBDQUEyQztBQUMzQyw2QkFBOEI7QUFDOUIsK0JBQWdDO0FBQ2hDLGdDQUFpQztBQUNqQyxxREFBa0Q7QUFDbEQsMkNBQXdDO0FBQ3hDLCtEQUE0RDtBQUM1RCxpREFBOEM7QUFDOUMscUNBQWtDO0FBQ2xDLHlEQUEwRDtBQUMxRCwyREFBd0Q7QUFDeEQsbUNBQTBDO0FBVzFDLE1BQU0sV0FBVyxHQUFzQixJQUFJLHFDQUFpQixFQUFFLENBQUM7QUFDL0QsTUFBTSxZQUFZLEdBQWtCLElBQUksTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDO0FBQ3hELE1BQU0sTUFBTSxHQUFpQixJQUFJLDJCQUFZLEVBQUUsQ0FBQztBQUNoRCxNQUFNLFVBQVUsR0FBZSxJQUFJLHVCQUFVLEVBQUUsQ0FBQztBQUNoRCxNQUFNLE9BQU8sR0FBWTtJQUN4QixJQUFJLEVBQUUsSUFBSSxXQUFJLEVBQUU7Q0FDaEIsQ0FBQztBQUNGLE1BQU0sT0FBTyxHQUEyQixFQUFFLENBQUM7QUFPM0MsU0FBUyxHQUFHO0lBQ1gsTUFBTSxTQUFTLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztJQUc3QixNQUFNLGNBQWMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDeEUsSUFBSSxDQUFDLGNBQWMsRUFBRTtRQUNwQixPQUFPLENBQUMsS0FBSyxDQUFDLDJDQUEyQyxDQUFDLENBQUE7UUFDMUQsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztLQUNoQjtJQUVELFdBQVcsQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUM1QixVQUFVLEVBQUUsQ0FBQztJQUNiLGVBQWUsRUFBRSxDQUFDO0lBQ2xCLFlBQVksRUFBRSxDQUFDO0lBQ2Ysc0JBQXNCLEVBQUUsQ0FBQztJQUN6QixhQUFhLEVBQUUsQ0FBQztJQUNoQixpQkFBaUIsRUFBRSxDQUFDO0lBR3BCLE1BQU0sU0FBUyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssT0FBTyxDQUFDLENBQUM7SUFHM0UsSUFBSSxTQUFTLEVBQUU7UUFDZCxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxtQkFBbUIsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUM1RCxPQUFPLENBQUMsS0FBSyxDQUFDLG9EQUFvRCxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7S0FDdEY7SUFHRCxPQUFPLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDakMsWUFBWSxFQUFFLENBQUM7SUFDZixhQUFhLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDNUMsYUFBYSxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBSzNDLE1BQU0sT0FBTyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUM7SUFDdEQsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLE9BQU8sQ0FBQztJQUNqQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLGFBQWEsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxlQUFlLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sZ0NBQWdDLE9BQU8sa0JBQWtCLENBQUMsQ0FBQztJQUdwTCxPQUFPLENBQUMsS0FBSyxDQUFDLG9DQUFvQyxDQUFDLENBQUM7SUFDcEQscUJBQXFCLEVBQUUsQ0FBQyxJQUFJLENBQUM7UUFHNUIsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDakUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2xCLEVBQUUsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdEIsT0FBTyxPQUFPLENBQUM7SUFFaEIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsT0FBZTtRQUdoQyxPQUFPLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDcEMsT0FBTyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUVwQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFHUCxPQUFPLENBQUMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLENBQUM7UUFDeEMsT0FBTyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBRTVDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUdQLE1BQU0sV0FBVyxHQUFXLENBQUMsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsT0FBTyxFQUFFLEdBQUcsU0FBUyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7UUFDM0UsSUFBSSxVQUFVLEdBQVcsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLENBQUM7UUFDeEQsSUFBSSxHQUFHLEdBQVcsNEJBQTRCLENBQUM7UUFDL0MsSUFBSSxJQUFJLEdBQVcsQ0FBQyxDQUFDO1FBQ3JCLElBQUksVUFBVSxJQUFJLEVBQUUsRUFBRTtZQUNyQixJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFDbkMsVUFBVSxJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUM7WUFDeEIsR0FBRyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxJQUFJLEdBQUcsT0FBTyxDQUFDO1NBQ3JEO1FBQ0QsSUFBSSxVQUFVLEdBQUcsQ0FBQztZQUNqQixHQUFHLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLFVBQVUsR0FBRyxXQUFXLENBQUM7YUFDckUsSUFBSSxJQUFJLElBQUksQ0FBQztZQUNqQixHQUFHLElBQUksa0JBQWtCLENBQUE7UUFDMUIsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUdsQixJQUFJLFNBQVMsRUFBRTtZQUNkLE9BQU8sQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQztZQUN0QyxhQUFhLEVBQUUsQ0FBQztZQUNoQixPQUFPO1NBQ1A7UUFHRCxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7SUFFaEIsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQztRQUNuQixPQUFPLENBQUMsS0FBSyxDQUFDLDBDQUEwQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzdELE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNmLE9BQU87SUFDUixDQUFDLENBQUMsQ0FBQTtBQUNILENBQUM7QUFDRCxHQUFHLEVBQUUsQ0FBQztBQUtOLFNBQVMsVUFBVTtJQUNsQixPQUFPLENBQUMsSUFBSSxDQUFDLDJCQUEyQixJQUFJLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztJQUN0RCxPQUFPLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztBQUNqQyxDQUFDO0FBR0QsU0FBUyxXQUFXLENBQUMsSUFBWTtJQUNoQyxJQUFJO1FBQ0gsTUFBTSxJQUFJLEdBQUcsRUFBRSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDNUMsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUdqQyxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztLQUM1QjtJQUFDLE9BQU8sQ0FBQyxFQUFFO1FBQ1gsT0FBTyxDQUFDLEtBQUssQ0FBQywyQ0FBMkMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM5RCxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0tBQ2hCO0FBQ0YsQ0FBQztBQUdELFNBQVMsZUFBZTtJQUN2QixNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxDQUFDO0lBQ3ZELE9BQU8sQ0FBQyxLQUFLLENBQUMsNEJBQTRCLElBQUksRUFBRSxDQUFDLENBQUM7SUFDbEQsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDO0lBQ2QsSUFBSSxFQUFFLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztRQUN0QixJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDOztRQUVqRCxPQUFPLENBQUMsSUFBSSxDQUFDLHlCQUF5QixJQUFJLGlCQUFpQixDQUFDLENBQUM7SUFHOUQsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN6QixPQUFPLENBQUMsS0FBSyxDQUFDLGNBQWMsRUFBRSxVQUFVLENBQUMsQ0FBQztBQUMzQyxDQUFDO0FBR0QsU0FBUyxZQUFZO0lBQ3BCLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQztJQUMxQyxPQUFPLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztBQUN6QyxDQUFDO0FBR0QsU0FBUyxzQkFBc0I7SUFDOUIsV0FBVyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDO0FBQ3ZDLENBQUM7QUFHRCxTQUFTLGFBQWE7SUFFckIsTUFBTSxRQUFRLEdBQUcsSUFBSSxlQUFNLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDdkMsUUFBUSxDQUFDLElBQUksR0FBRyxVQUFVLElBQVksRUFBRSxJQUFZO1FBQ25ELE9BQU8sV0FBVyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDO0lBQ2pELENBQUMsQ0FBQTtJQUVELGVBQU0sQ0FBQyxVQUFVLENBQUM7UUFDakIsUUFBUSxFQUFFLFFBQVE7S0FDbEIsQ0FBQyxDQUFDO0FBQ0osQ0FBQztBQUdELFNBQVMsaUJBQWlCO0lBSXpCLE1BQU0sZUFBZSxHQUErQjtRQUNuRCxHQUFHLEVBQUUsT0FBTztRQUNaLEdBQUcsRUFBRSxNQUFNO1FBQ1gsR0FBRyxFQUFFLE1BQU07UUFDWCxHQUFHLEVBQUUsUUFBUTtRQUdiLEdBQUcsRUFBRSxRQUFRO1FBQ2IsR0FBRyxFQUFFLFNBQVM7UUFDZCxHQUFHLEVBQUUsT0FBTztRQUNaLEdBQUcsRUFBRSxRQUFRO1FBQ2IsR0FBRyxFQUFFLFFBQVE7UUFDYixHQUFHLEVBQUUsT0FBTztLQUNaLENBQUE7SUFHRCxJQUFJLFdBQVcsR0FBRyxHQUFHLENBQUM7SUFDdEIsS0FBSyxJQUFJLElBQUksSUFBSSxlQUFlO1FBQy9CLFdBQVcsSUFBSSxJQUFJLENBQUM7SUFDckIsV0FBVyxJQUFJLEdBQUcsQ0FBQztJQUNuQixNQUFNLFFBQVEsR0FBVyxJQUFJLE1BQU0sQ0FBQyxXQUFXLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFHdEQsU0FBUyxhQUFhLENBQUMsYUFBcUI7UUFDM0MsT0FBTyxlQUFlLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUdELFlBQVksQ0FBQyxlQUFlLENBQUM7UUFHNUIsbUJBQW1CLEVBQUUsVUFBVSxJQUFtQjtZQUNqRCxJQUFJLENBQUMsR0FBUyxDQUFDLE9BQU8sSUFBSSxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ2pFLE9BQU8sQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3hCLENBQUM7UUFHRCxZQUFZLEVBQUUsVUFBVSxLQUFhO1lBQ3BDLE9BQU8sS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDL0MsQ0FBQztRQUdELFlBQVksRUFBRSxVQUFVLEtBQWE7WUFDcEMsT0FBTyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZELENBQUM7UUFHRCxRQUFRLEVBQUUsVUFBVSxLQUFhO1lBQ2hDLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZELENBQUM7S0FDRCxDQUFDLENBQUM7SUFHSCxlQUFlLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztJQUMxQyxZQUFZLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxlQUFlLENBQUMsQ0FBQztJQUN2RCxZQUFZLENBQUMsV0FBVyxDQUFDLG1CQUFtQixFQUFFLGlDQUFlLENBQUMsQ0FBQztJQUcvRCxNQUFNLFdBQVcsR0FBVyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUNoRixNQUFNLEdBQUcsR0FBMkIsSUFBSSxNQUFNLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNwRixZQUFZLENBQUMsa0JBQWtCLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDdEMsQ0FBQztBQUdELFNBQVMsWUFBWTtJQUVwQixNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUNoRSxJQUFJLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBRTtRQUN6QixPQUFPLENBQUMsSUFBSSxDQUFDLHNDQUFzQyxJQUFJLGlCQUFpQixDQUFDLENBQUM7UUFDMUUsT0FBTztLQUNQO0lBR0QsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxRQUFnQjtRQUd0RCxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDdkMsT0FBTyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsUUFBUSxNQUFNLElBQUksbUVBQW1FLENBQUMsQ0FBQztZQUMxSCxPQUFPO1NBQ1A7UUFFRCxNQUFNLFdBQVcsR0FBVyxFQUFFLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ2hGLE1BQU0sVUFBVSxHQUFXLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLFFBQVEsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUM1RSxPQUFPLENBQUMsVUFBVSxDQUFDLEdBQUcsV0FBVyxDQUFDO0lBQ25DLENBQUMsQ0FBQyxDQUFDO0FBQ0osQ0FBQztBQUdELFNBQVMsYUFBYSxDQUFDLEdBQVcsRUFBRSxFQUFhO0lBRWhELE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDN0MsSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUU7UUFDekIsT0FBTyxDQUFDLElBQUksQ0FBQyxZQUFZLElBQUksaUJBQWlCLENBQUMsQ0FBQztRQUNoRCxPQUFPO0tBQ1A7SUFHRCxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLFFBQWdCO1FBR3RELElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUN2QyxPQUFPLENBQUMsSUFBSSxDQUFDLHNCQUFzQixRQUFRLE1BQU0sSUFBSSx3REFBd0QsQ0FBQyxDQUFDO1lBQy9HLE9BQU87U0FDUDtRQUdELE1BQU0sT0FBTyxHQUFHLFlBQVksQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDN0MsSUFBSSxPQUFPLEVBQUU7WUFFWixZQUFZLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdEIsRUFBRSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUNqQjtJQUNGLENBQUMsQ0FBQyxDQUFDO0lBRUgsRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztBQUN2QixDQUFDO0FBR0QsU0FBUyxZQUFZLENBQUMsSUFBWSxFQUFFLFFBQWdCO0lBRW5ELE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQzNDLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1FBQzdCLE9BQU8sQ0FBQyxJQUFJLENBQUMsMkJBQTJCLFFBQVEsNkJBQTZCLENBQUMsQ0FBQztRQUMvRSxPQUFPLFNBQVMsQ0FBQztLQUNqQjtJQUdELE1BQU0sV0FBVyxHQUFXLEVBQUUsQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQy9ELE1BQU0sT0FBTyxHQUFZLElBQUksaUJBQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUM7SUFDL0UsT0FBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsV0FBVyxDQUFDLENBQUM7SUFFNUMsT0FBTyxPQUFPLENBQUM7QUFDaEIsQ0FBQztBQUdELFNBQVMsWUFBWSxDQUFDLE9BQWdCO0lBQ3JDLElBQUksT0FBTyxDQUFDLElBQUksSUFBSSxJQUFJLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQztRQUNuRCxPQUFPO0lBRVIsTUFBTSxHQUFHLEdBQVcsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDeEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDLEVBQUUsRUFBRTtRQUM3QixNQUFNLENBQUMsR0FBVyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ2YsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3BDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBRSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUNyRDs7WUFFQSxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0tBQ2xDO0FBQ0YsQ0FBQztBQUlELFNBQVMscUJBQXFCO0lBRTdCLElBQUksUUFBUSxHQUFHLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUdqQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFhO1FBR2pELFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO1lBQ3hCLE9BQU8sZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQyxDQUFDLENBQUM7SUFHSCxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFhO1FBR2pELFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO1lBQ3hCLE9BQU8sZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQyxDQUFDLENBQUM7SUFHSCxPQUFPLFFBQVEsQ0FBQztBQUNqQixDQUFDO0FBR0QsU0FBUyxlQUFlLENBQUMsT0FBZ0I7SUFDeEMsT0FBTyxDQUFDLElBQUksR0FBRyxPQUFPLENBQUM7SUFDdkIsT0FBTyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDO0lBQ2xDLE9BQU8sWUFBWSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLE1BQU07UUFHakYsT0FBTyxDQUFDLEtBQUssQ0FBQyw4QkFBOEIsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7UUFDaEUsT0FBTyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFHekIsSUFBSSxPQUFPLENBQUMsVUFBVSxFQUFFO1lBQ3ZCLE9BQU8sQ0FBQyxLQUFLLENBQUMsNkJBQTZCLE9BQU8sQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDO1lBQ2hFLE9BQU8sQ0FBQyxPQUFPLEdBQUcsSUFBQSxlQUFNLEVBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQzFDO0lBQ0YsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBUTtRQUMxQixPQUFPLENBQUMsS0FBSyxDQUFDLDRCQUE0QixPQUFPLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDakUsTUFBTSxDQUFDLENBQUM7SUFDVCxDQUFDLENBQUMsQ0FBQTtBQUNILENBQUM7QUFHRCxTQUFTLFFBQVEsQ0FBQyxHQUFXO0lBQzVCLElBQUksRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFdBQVcsRUFBRSxFQUFFO1FBQ3pELE9BQU8sQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztLQUNmO0FBQ0YsQ0FBQztBQUlELFNBQVMsa0JBQWtCLENBQUMsUUFBZ0I7SUFFM0MsSUFBSSxRQUFRLEdBQUcsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBR2pDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQWE7UUFHakQsUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7WUFDeEIsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDaEMsT0FBTyxZQUFZLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLEdBQUcsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDO1FBQ25GLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQyxDQUFDLENBQUM7SUFHSCxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFhO1FBR2pELFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO1lBQ3hCLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQ2hDLE9BQU8sWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxHQUFHLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQztRQUNuRixDQUFDLENBQUMsQ0FBQztJQUNKLENBQUMsQ0FBQyxDQUFDO0lBR0gsT0FBTyxRQUFRLENBQUM7QUFDakIsQ0FBQztBQUdELFNBQVMsWUFBWSxDQUFDLE9BQWdCLEVBQUUsSUFBWSxFQUFFLFFBQWdCO0lBRXJFLElBQUksT0FBTyxJQUFJLElBQUk7UUFDbEIsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksS0FBSyxDQUFDLG1DQUFtQyxRQUFRLHFCQUFxQixDQUFDLENBQUMsQ0FBQztJQUdwRyxJQUFJLE9BQU8sQ0FBQyxXQUFXLEVBQUU7UUFFeEIsTUFBTSxNQUFNLEdBQXVCLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7UUFDMUYsSUFBSSxNQUFNLEVBQUU7WUFDWCxPQUFPLENBQUMsSUFBSSxHQUFHLE9BQU8sQ0FBQztZQUN2QixPQUFPLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUM7WUFDbEMsT0FBTyxZQUFZLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxNQUFNO2dCQUN4RSxPQUFPLENBQUMsS0FBSyxDQUFDLGVBQWUsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7Z0JBQzVDLEVBQUUsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQztZQUM3QyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFRO2dCQUMxQixPQUFPLENBQUMsS0FBSyxDQUFDLHlCQUF5QixPQUFPLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pELE1BQU0sQ0FBQyxDQUFDO1lBQ1QsQ0FBQyxDQUFDLENBQUE7U0FDRjthQUNJO1lBRUosT0FBTyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUNwQyxPQUFPLENBQUMsS0FBSyxDQUFDLGVBQWUsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7Z0JBQzVDLEVBQUUsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDdEQsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBUTtnQkFDMUIsT0FBTyxDQUFDLEtBQUssQ0FBQyx5QkFBeUIsT0FBTyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUN6RCxNQUFNLENBQUMsQ0FBQztZQUNULENBQUMsQ0FBQyxDQUFBO1NBQ0Y7S0FDRDtTQUNJO1FBQ0osT0FBTyxDQUFDLEtBQUssQ0FBQyxlQUFlLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQzVDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDMUIsT0FBTyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7S0FDekI7QUFDRixDQUFDO0FBR0QsU0FBUyxrQkFBa0IsQ0FBQyxHQUFXLEVBQUUsUUFBd0I7SUFFaEUsSUFBSSxDQUFDLFFBQVE7UUFDWixRQUFRLEdBQUcsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBRTlCLEVBQUUsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsUUFBZ0I7UUFHckQsTUFBTSxJQUFJLEdBQVcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFHOUMsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBRTVCLElBQUksVUFBVSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUU7Z0JBQy9DLE9BQU8sQ0FBQyxLQUFLLENBQUMsWUFBWSxJQUFJLGdEQUFnRCxDQUFDLENBQUM7Z0JBQ2hGLE9BQU87YUFDUDtZQUNELE9BQU8sQ0FBQyxLQUFLLENBQUMsYUFBYSxJQUFJLG9DQUFvQyxDQUFDLENBQUM7U0FDckU7UUFHRCxJQUFJLFVBQVUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFO1lBQy9DLE9BQU8sQ0FBQyxLQUFLLENBQUMsWUFBWSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxRQUFRLENBQUMsb0NBQW9DLENBQUMsQ0FBQztZQUN4RixPQUFPO1NBQ1A7UUFHRCxJQUFJLEVBQUUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsV0FBVyxFQUFFLEVBQUU7WUFDcEMsSUFBSSxRQUFRLElBQUksT0FBTztnQkFDdEIsT0FBTztZQUNSLGtCQUFrQixDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztTQUNuQzthQUNJO1lBRUosTUFBTSxXQUFXLEdBQVcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDL0UsTUFBTSxPQUFPLEdBQVcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3BGLE1BQU0sUUFBUSxHQUFXLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQ3RELFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxPQUFPLENBQUMsRUFBRSxXQUFXLENBQUMsQ0FBQztZQUc5RCxNQUFNLE9BQU8sR0FBRyxZQUFZLENBQUMsR0FBRyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQzVDLElBQUksT0FBTyxFQUFFO2dCQUVaLElBQUksT0FBTyxDQUFDLEdBQUcsSUFBSSxhQUFhO29CQUMvQixPQUFPLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztnQkFHbkIsSUFBSSxPQUFPLENBQUMsV0FBVztvQkFDdEIsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUdsQyxRQUFRLEdBQUcsUUFBUyxDQUFDLElBQUksQ0FBQztvQkFDekIsT0FBTyxZQUFZLENBQUMsT0FBTyxFQUFFLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDOUMsQ0FBQyxDQUFDLENBQUM7YUFDSDtTQUNEO0lBQ0YsQ0FBQyxDQUFDLENBQUM7SUFFSCxPQUFPLFFBQVEsQ0FBQztBQUNqQixDQUFDO0FBR0QsU0FBUyxTQUFTLENBQUMsTUFBYyxFQUFFLE9BQWU7SUFDakQsRUFBRSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7QUFDbEMsQ0FBQztBQUdELFNBQVMsV0FBVyxDQUFDLElBQVksRUFBRSxJQUFZO0lBQzlDLElBQUksSUFBSSxHQUFXLEdBQUcsSUFBSSxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztJQUN4QyxNQUFNLEtBQUssR0FBYSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDM0YsTUFBTSxHQUFHLEdBQVcsS0FBSyxDQUFDLE1BQU0sQ0FBQztJQUNqQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUMsRUFBRSxFQUFFO1FBRTdCLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDOUMsT0FBTztRQUVSLElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDakMsSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ3ZCLEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDcEI7QUFDRixDQUFDO0FBR0QsU0FBUyxZQUFZLENBQUMsQ0FBVSxFQUFFLENBQVU7SUFDM0MsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7QUFDNUMsQ0FBQztBQUdELFNBQVMsdUJBQXVCLENBQUMsUUFBZ0I7SUFDaEQsTUFBTSxLQUFLLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN4QyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUM7UUFDZCxPQUFPLEtBQUssQ0FBQztJQUVkLE1BQU0sR0FBRyxHQUFXLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ2xELE9BQU8sQ0FBQyxHQUFHLElBQUksTUFBTSxJQUFJLEdBQUcsSUFBSSxLQUFLLElBQUksR0FBRyxJQUFJLElBQUksSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLENBQUM7QUFDNUUsQ0FBQztBQUdELFNBQVMsYUFBYTtJQUNyQixNQUFNLFNBQVMsR0FBVyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUc3RSxNQUFNLElBQUksR0FBRyxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFO1FBQzdDLEtBQUssRUFBRSxDQUFDO1FBQ1IsSUFBSSxFQUFFLElBQUk7S0FDVixDQUFDLENBQUM7SUFHSCxJQUFJLE1BQU0sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFO1FBQzFCLE1BQU0sT0FBTyxHQUFXLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDcEUsTUFBTSxNQUFNLEdBQVksRUFBRSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ1osT0FBTyxDQUFDLEtBQUssQ0FBQywyQkFBMkIsT0FBTyxpQkFBaUIsQ0FBQyxDQUFDO1lBQ25FLE1BQU0sQ0FBQyxHQUFHLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQztTQUNsQzthQUNJLElBQUksRUFBRSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxXQUFXLEVBQUUsRUFBRTtZQUM1QyxJQUFJLE1BQU0sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRztnQkFDeEUsTUFBTSxDQUFDLEdBQUcsQ0FBQyxVQUFVLElBQUksWUFBWSxDQUFDOztnQkFFdEMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxVQUFVLElBQUksYUFBYSxDQUFDO1NBQ3hDO1FBR0QsSUFBSSxNQUFNLENBQUMsR0FBRyxDQUFDLFVBQVUsSUFBSSxNQUFNLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRztZQUNsRSxNQUFNLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRyxJQUFJLE1BQU0sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLENBQUM7S0FDckQ7SUFHRCxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsT0FBTyxFQUFFLFFBQVE7UUFDNUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUU7WUFHMUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFLFVBQVUsR0FBRztnQkFFMUMsSUFBSSxHQUFHLEVBQUU7b0JBQ1IsT0FBTyxDQUFDLEtBQUssQ0FBQyw4QkFBOEIsT0FBTyxDQUFDLEdBQUcsTUFBTSxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztvQkFHNUUsSUFBSSxNQUFNLENBQUMsR0FBRyxDQUFDLFVBQVUsSUFBSSxPQUFPLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxDQUFDLElBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxHQUFHO3dCQUN6SixJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFDO3lCQUM5RDt3QkFDSixRQUFRLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO3dCQUM1QyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUM7cUJBQ2Y7aUJBQ0Q7WUFDRixDQUFDLENBQUMsQ0FBQztRQUNKLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2IsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7SUFFOUIsT0FBTyxDQUFDLElBQUksQ0FBQywyQkFBMkIsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO0FBQzdELENBQUMifQ==