"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigHelper = void 0;
const path_1 = __importDefault(require("path"));
class ConfigHelper {
    constructor() {
        this.m_src = {
            path: '',
        };
        this.m_meta = {};
        this.m_opengraph = {};
        this.m_highlight = {
            parentClassName: 'highlight',
            shouldWrap: true,
        };
        this.m_server = {
            port: 4000,
        };
    }
    get src() { return this.m_src; }
    get meta() { return this.m_meta; }
    get opengraph() { return this.m_opengraph; }
    get highlight() { return this.m_highlight; }
    get server() { return this.m_server; }
    parse(json, jsonPath) {
        this._parseSrcConfig(json, jsonPath);
        this._parseMetaConfig(json);
        this._parseOpenGraphConfig(json);
        this._parseHighlightConfig(json);
        this._parseServerConfig(json);
    }
    _isString(val) {
        const typeOk = typeof (val) === 'string' || val instanceof String;
        if (!typeOk) {
            return typeOk;
        }
        const str = val;
        return !!str && str.trim().length > 0;
    }
    _isInt(val) {
        if (typeof (val) !== 'number' || isNaN(val)) {
            return false;
        }
        return Number.isSafeInteger(val);
    }
    _isBoolean(val) {
        return (typeof (val) !== 'boolean');
    }
    _parseSrcConfig(json, jsonPath) {
        const lastIndex = (jsonPath.indexOf('/') != -1) ? jsonPath.lastIndexOf('/') : jsonPath.lastIndexOf(path_1.default.sep);
        this.m_src.path = (lastIndex != -1) ? jsonPath.substring(0, lastIndex) : jsonPath;
        if (json.src && json.src['404'] && this._isString(json.src['404'])) {
            this.m_src.fourOhFour = json.src['404'];
        }
    }
    _parseMetaConfig(json) {
        if (json.meta && json.meta.keywords && this._isString(json.meta.keywords)) {
            this.m_meta.keywords = json.meta.keywords;
        }
        if (json.meta && json.meta.description && this._isString(json.meta.description)) {
            this.m_meta.description = json.meta.description;
        }
    }
    _parseOpenGraphConfig(json) {
        if (json.opengraph && json.opengraph['fb:admins'] && this._isString(json.opengraph['fb:admins'])) {
            this.m_opengraph['fb:admins'] = json.opengraph['fb:admins'];
        }
        if (json.opengraph && json.opengraph['og:type'] && this._isString(json.opengraph['og:type'])) {
            this.m_opengraph['og:type'] = json.opengraph['og:type'];
        }
        if (json.opengraph && json.opengraph['og:image'] && this._isString(json.opengraph['og:image'])) {
            this.m_opengraph['og:image'] = json.opengraph['og:image'];
        }
    }
    _parseHighlightConfig(json) {
        if (json.highlight && json.highlight.parentClassName && this._isString(json.highlight.parentClassName)) {
            this.m_highlight.parentClassName = json.highlight.parentClassName;
        }
        if (json.highlight && this._isBoolean(json.highlight.shouldWrap)) {
            this.m_highlight.shouldWrap = !!json.highlight.shouldWrap;
        }
    }
    _parseServerConfig(json) {
        if (json.server && json.server.port && this._isInt(json.server.port)) {
            this.m_server.port = json.server.port;
        }
    }
}
exports.ConfigHelper = ConfigHelper;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29uZmlnSGVscGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiQ29uZmlnSGVscGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLGdEQUF3QjtBQTJCeEIsTUFBYSxZQUFZO0lBQXpCO1FBR1MsVUFBSyxHQUFjO1lBQzFCLElBQUksRUFBRSxFQUFFO1NBQ1IsQ0FBQztRQUNNLFdBQU0sR0FBZSxFQUFFLENBQUM7UUFDeEIsZ0JBQVcsR0FBb0IsRUFBRSxDQUFDO1FBQ2xDLGdCQUFXLEdBQW9CO1lBQ3RDLGVBQWUsRUFBRSxXQUFXO1lBQzVCLFVBQVUsRUFBRSxJQUFJO1NBQ2hCLENBQUM7UUFDTSxhQUFRLEdBQWlCO1lBQ2hDLElBQUksRUFBRSxJQUFJO1NBQ1YsQ0FBQztJQXVISCxDQUFDO0lBaEhBLElBQVcsR0FBRyxLQUFnQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBS2xELElBQVcsSUFBSSxLQUFpQixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO0lBS3JELElBQVcsU0FBUyxLQUFzQixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO0lBS3BFLElBQVcsU0FBUyxLQUFzQixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO0lBS3BFLElBQVcsTUFBTSxLQUFtQixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBU3BELEtBQUssQ0FBQyxJQUFhLEVBQUUsUUFBZ0I7UUFDM0MsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqQyxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDakMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFLTyxTQUFTLENBQUMsR0FBUTtRQUN6QixNQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssUUFBUSxJQUFJLEdBQUcsWUFBWSxNQUFNLENBQUM7UUFDbEUsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNaLE9BQU8sTUFBTSxDQUFDO1NBQ2Q7UUFDRCxNQUFNLEdBQUcsR0FBRyxHQUFhLENBQUM7UUFDMUIsT0FBTyxDQUFDLENBQUMsR0FBRyxJQUFJLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFHTyxNQUFNLENBQUMsR0FBUTtRQUN0QixJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxRQUFRLElBQUksS0FBSyxDQUFDLEdBQWEsQ0FBQyxFQUFFO1lBQ3RELE9BQU8sS0FBSyxDQUFDO1NBQ2I7UUFDRCxPQUFPLE1BQU0sQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUdPLFVBQVUsQ0FBQyxHQUFRO1FBQzFCLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssU0FBUyxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUdPLGVBQWUsQ0FBQyxJQUFhLEVBQUUsUUFBZ0I7UUFFdEQsTUFBTSxTQUFTLEdBQUcsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsY0FBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzdHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLENBQUMsU0FBUyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUM7UUFDbEYsSUFBSSxJQUFJLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDbkUsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQVcsQ0FBQztTQUNsRDtJQUNGLENBQUM7SUFHTyxnQkFBZ0IsQ0FBQyxJQUFhO1FBQ3JDLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDMUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFrQixDQUFDO1NBQ3BEO1FBQ0QsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRTtZQUNoRixJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQXFCLENBQUM7U0FDMUQ7SUFDRixDQUFDO0lBR08scUJBQXFCLENBQUMsSUFBYTtRQUMxQyxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRTtZQUNqRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFXLENBQUM7U0FDdEU7UUFDRCxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRTtZQUM3RixJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFXLENBQUM7U0FDbEU7UUFDRCxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsRUFBRTtZQUMvRixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFXLENBQUM7U0FDcEU7SUFDRixDQUFDO0lBR08scUJBQXFCLENBQUMsSUFBYTtRQUMxQyxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxFQUFFO1lBQ3ZHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsZUFBeUIsQ0FBQztTQUM1RTtRQUNELElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDakUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDO1NBQzFEO0lBQ0YsQ0FBQztJQUdPLGtCQUFrQixDQUFDLElBQWE7UUFDdkMsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNyRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQWMsQ0FBQztTQUNoRDtJQUNGLENBQUM7Q0FFRDtBQXJJRCxvQ0FxSUMifQ==