"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Site = void 0;
class Site {
    constructor() {
        this.title = '';
        this.email = '';
        this.baseurl = "";
        this.url = '';
        this.posts = [];
        this.pages = [];
        this.tags = {
            size: 0,
        };
    }
    updateFromYAML(yaml) {
        for (var key in yaml) {
            if (key in this)
                this[key] = yaml[key];
        }
    }
}
exports.Site = Site;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2l0ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlNpdGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBZUEsTUFBYSxJQUFJO0lBQWpCO1FBT1EsVUFBSyxHQUFXLEVBQUUsQ0FBQztRQUtuQixVQUFLLEdBQVcsRUFBRSxDQUFDO1FBS25CLFlBQU8sR0FBVyxFQUFFLENBQUM7UUFLckIsUUFBRyxHQUFXLEVBQUUsQ0FBQztRQUtqQixVQUFLLEdBQWMsRUFBRSxDQUFDO1FBS3RCLFVBQUssR0FBYyxFQUFFLENBQUM7UUFLdEIsU0FBSSxHQUFhO1lBQ3ZCLElBQUksRUFBRSxDQUFDO1NBQ1AsQ0FBQztJQXlCSCxDQUFDO0lBUE8sY0FBYyxDQUFDLElBQWdCO1FBQ3JDLEtBQUssSUFBSSxHQUFHLElBQUksSUFBSSxFQUFFO1lBQ3JCLElBQUksR0FBRyxJQUFJLElBQUk7Z0JBQ2QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUN2QjtJQUNGLENBQUM7Q0FFRDtBQWhFRCxvQkFnRUMifQ==