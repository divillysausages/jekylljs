import path from "path";

export type ConfigSrc = {
	path: string,
	fourOhFour?: string,
}
export type ConfigMeta = {
	keywords?: string,
	description?: string,
}
export type ConfigOpenGraph = {
	"fb:admins"?: string;
	"og:type"?: string;
	"og:image"?: string,
}
export type ConfigHighlight = {
	parentClassName: string,
	shouldWrap: boolean,
}
export type ConfigServer = {
	port: number,
}

type JSONObj = {
	[key: string]: any;
}

export class ConfigHelper {
	/************************************************************/

	private m_src: ConfigSrc = {
		path: '',
	}; // the config for our src dir
	private m_meta: ConfigMeta = {}; // the config for our meta data
	private m_opengraph: ConfigOpenGraph = {};	// the config for our opengraph data
	private m_highlight: ConfigHighlight = {
		parentClassName: 'highlight',
		shouldWrap: true,
	}; // the config for our highlight tags
	private m_server: ConfigServer = {
		port: 4000,
	};	// the config for our serving port

	/************************************************************/

	/**
	 * The config for our src dir
	 */
	public get src(): ConfigSrc { return this.m_src; }

	/**
	 * The config for our meta data
	 */
	public get meta(): ConfigMeta { return this.m_meta; }

	/**
	 * The config for our default opengraph data
	 */
	public get opengraph(): ConfigOpenGraph { return this.m_opengraph; }

	/**
	 * The config for our highlighter
	 */
	public get highlight(): ConfigHighlight { return this.m_highlight; }

	/**
	 * The config for our server
	 */
	public get server(): ConfigServer { return this.m_server; }

	/************************************************************/

	/**
	 * Parses the config file, extracting all our data
	 * @param json The loaded json object
	 * @param jsonPath The path the we loaded our json from
	 */
	public parse(json: JSONObj, jsonPath: string): void {
		this._parseSrcConfig(json, jsonPath);
		this._parseMetaConfig(json);
		this._parseOpenGraphConfig(json);
		this._parseHighlightConfig(json);
		this._parseServerConfig(json);
	}

	/************************************************************/

	// checks if a value is a valid string
	private _isString(val: any): boolean {
		const typeOk = typeof (val) === 'string' || val instanceof String;
		if (!typeOk) {
			return typeOk;
		}
		const str = val as string;
		return !!str && str.trim().length > 0;
	}

	// checks if a value is a valid int
	private _isInt(val: any): boolean {
		if (typeof (val) !== 'number' || isNaN(val as number)) {
			return false;
		}
		return Number.isSafeInteger(val);
	}

	// checks if a value is a valid boolean
	private _isBoolean(val: any): boolean {
		return (typeof (val) !== 'boolean');
	}

	// parses our src dir config
	private _parseSrcConfig(json: JSONObj, jsonPath: string): void {
		// our src path is the folder that our config was in
		const lastIndex = (jsonPath.indexOf('/') != -1) ? jsonPath.lastIndexOf('/') : jsonPath.lastIndexOf(path.sep);
		this.m_src.path = (lastIndex != -1) ? jsonPath.substring(0, lastIndex) : jsonPath;
		if (json.src && json.src['404'] && this._isString(json.src['404'])) {
			this.m_src.fourOhFour = json.src['404'] as string;
		}
	}

	// parses our meta config
	private _parseMetaConfig(json: JSONObj): void {
		if (json.meta && json.meta.keywords && this._isString(json.meta.keywords)) {
			this.m_meta.keywords = json.meta.keywords as string;
		}
		if (json.meta && json.meta.description && this._isString(json.meta.description)) {
			this.m_meta.description = json.meta.description as string;
		}
	}

	// parses our opengraph config
	private _parseOpenGraphConfig(json: JSONObj): void {
		if (json.opengraph && json.opengraph['fb:admins'] && this._isString(json.opengraph['fb:admins'])) {
			this.m_opengraph['fb:admins'] = json.opengraph['fb:admins'] as string;
		}
		if (json.opengraph && json.opengraph['og:type'] && this._isString(json.opengraph['og:type'])) {
			this.m_opengraph['og:type'] = json.opengraph['og:type'] as string;
		}
		if (json.opengraph && json.opengraph['og:image'] && this._isString(json.opengraph['og:image'])) {
			this.m_opengraph['og:image'] = json.opengraph['og:image'] as string;
		}
	}

	// parses our highlight config
	private _parseHighlightConfig(json: JSONObj): void {
		if (json.highlight && json.highlight.parentClassName && this._isString(json.highlight.parentClassName)) {
			this.m_highlight.parentClassName = json.highlight.parentClassName as string;
		}
		if (json.highlight && this._isBoolean(json.highlight.shouldWrap)) {
			this.m_highlight.shouldWrap = !!json.highlight.shouldWrap;
		}
	}

	// parses our server config
	private _parseServerConfig(json: JSONObj): void {
		if (json.server && json.server.port && this._isInt(json.server.port)) {
			this.m_server.port = json.server.port as number;
		}
	}

}