"use strict";
const Liquid = require("liquid");
class LiquidHighlight extends Liquid.Block {
    constructor(template, tagname, markup) {
        super(template, tagname, markup);
        this._lang = '';
        this._lang = (markup != null) ? markup.trim() : '';
    }
    render(context) {
        var lh = this;
        return super.render(context).then(function (ar) {
            const code = Liquid.Helpers.toFlatString(ar);
            return (LiquidHighlight.highlighter) ? LiquidHighlight.highlighter.highlight(code, lh._lang) : code;
        });
    }
}
module.exports = LiquidHighlight;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTGlxdWlkSGlnaGxpZ2h0LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiTGlxdWlkSGlnaGxpZ2h0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFDQSxpQ0FBa0M7QUFNbEMsTUFBTSxlQUFnQixTQUFRLE1BQU0sQ0FBQyxLQUFLO0lBcUJ6QyxZQUFZLFFBQWEsRUFBRSxPQUFlLEVBQUUsTUFBYztRQUN6RCxLQUFLLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztRQVgxQixVQUFLLEdBQUcsRUFBRSxDQUFDO1FBWWxCLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQ3BELENBQUM7SUFNTSxNQUFNLENBQUMsT0FBWTtRQUN6QixJQUFJLEVBQUUsR0FBb0IsSUFBSSxDQUFDO1FBRy9CLE9BQU8sS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFZO1lBQ3ZELE1BQU0sSUFBSSxHQUFXLE1BQU0sQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3JELE9BQU8sQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUNyRyxDQUFDLENBQUMsQ0FBQztJQUNKLENBQUM7Q0FDRDtBQUVELGlCQUFTLGVBQWUsQ0FBQyJ9