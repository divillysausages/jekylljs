/**
 * Describes some of the basic config that we can have with _config.yml
 */
export class YAMLConfig {

	/************************************************************/

	/**
	 * The source directory
	 */
	public source: string = ".";

	/**
	 * The destination directory
	 */
	public destination: string = "./_site";

	/**
	 * Where to find the layouts
	 */
	public layouts_dir: string = "./_layouts";

	/**
	 * Where to find our includes
	 */
	public includes_dir: string = "./_includes";

	/**
	 * What files to always include
	 */
	public include: string[] = [".htaccess"];

	/**
	 * What files to exclude when generating the site
	 */
	public exclude: string[] = [];

	/**
	 * The default site title
	 */
	public title: string = '';

	/**
	 * The email of the author of the site
	 */
	public email: string = '';

	/**
	 * The baseurl for the site. Useful if we're under a sub-domain
	 */
	public baseurl: string = "";

	/**
	 * The URL for the site
	 */
	public url: string = '';

	/************************************************************/

	/**
	 * Fills our data from a generic Javascript object
	 */
	public fromObj(obj: Object): void {
		for (var key in obj)
			this[key] = obj[key];
	}

}