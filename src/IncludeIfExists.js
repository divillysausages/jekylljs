"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IncludeIfExists = void 0;
const Liquid = require("liquid");
const Syntax = /((?:{{2}\s?)?[^%}]+(?:\s?}{2})?)/i;
const SyntaxHelp = "Syntax Error in 'include_if_exists' - Valid syntax: {% include_if_exists {{ path }} %} or {% include_if_exists path %}";
class IncludeIfExists extends Liquid.Tag {
    constructor(template, tagName, markup) {
        super(template, tagName, markup);
        this.filepath = '';
        const match = Syntax.exec(markup);
        if (!match) {
            throw new Liquid.SyntaxError(SyntaxHelp);
        }
        this.filepath = match[1].trim();
    }
    async subTemplate(context) {
        if (this.filepath.startsWith('{{') && this.filepath.endsWith('}}')) {
            const pathTemplate = await this.template.engine.parse(this.filepath);
            this.filepath = await pathTemplate.render(context);
        }
        const indexLastDot = this.filepath.lastIndexOf('.');
        const indexLastSlash = this.filepath.lastIndexOf('/');
        if (indexLastDot != -1 && indexLastDot > indexLastSlash) {
            this.filepath = this.filepath.substring(0, indexLastDot);
        }
        const src = await this.template
            .engine
            .fileSystem
            .readTemplateFile(this.filepath);
        return this.template.engine.parse(src);
    }
    async render(context) {
        return Promise.resolve()
            .then(() => this.subTemplate(context))
            .then(template => template.render(context))
            .catch(err => {
            if (err != null) {
                if (err.name === 'Liquid.FileSystemError' && err.message != null && err.message.indexOf('ENOENT') != -1)
                    return '';
                if (err.name === 'Liquid.ArgumentError' && err.message != null && err.message.indexOf('Illegal template name') != -1)
                    return '';
                throw err;
            }
        });
    }
}
exports.IncludeIfExists = IncludeIfExists;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSW5jbHVkZUlmRXhpc3RzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiSW5jbHVkZUlmRXhpc3RzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUNBLGlDQUFrQztBQUVsQyxNQUFNLE1BQU0sR0FBRyxtQ0FBbUMsQ0FBQTtBQUNsRCxNQUFNLFVBQVUsR0FBRyx3SEFBd0gsQ0FBQTtBQUUzSSxNQUFhLGVBQWdCLFNBQVEsTUFBTSxDQUFDLEdBQUc7SUFJOUMsWUFBWSxRQUFRLEVBQUUsT0FBTyxFQUFFLE1BQU07UUFDcEMsS0FBSyxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUE7UUFIMUIsYUFBUSxHQUFHLEVBQUUsQ0FBQztRQUtwQixNQUFNLEtBQUssR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO1FBQ2pDLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDWCxNQUFNLElBQUksTUFBTSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQTtTQUN4QztRQUVELElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFBO0lBQ2hDLENBQUM7SUFFRCxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU87UUFFeEIsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNuRSxNQUFNLFlBQVksR0FBRyxNQUFNLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDckUsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLFlBQVksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUE7U0FDbEQ7UUFHRCxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUNuRCxNQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUNyRCxJQUFJLFlBQVksSUFBSSxDQUFDLENBQUMsSUFBSSxZQUFZLEdBQUcsY0FBYyxFQUFFO1lBQ3hELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLFlBQVksQ0FBQyxDQUFBO1NBQ3hEO1FBRUQsTUFBTSxHQUFHLEdBQUcsTUFBTSxJQUFJLENBQUMsUUFBUTthQUM3QixNQUFNO2FBQ04sVUFBVTthQUNWLGdCQUFnQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQTtRQUNqQyxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQTtJQUN2QyxDQUFDO0lBRUQsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPO1FBQ25CLE9BQU8sT0FBTyxDQUFDLE9BQU8sRUFBRTthQUN0QixJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUNyQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQzFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUNaLElBQUksR0FBRyxJQUFJLElBQUksRUFBRTtnQkFFaEIsSUFBSSxHQUFHLENBQUMsSUFBSSxLQUFLLHdCQUF3QixJQUFJLEdBQUcsQ0FBQyxPQUFPLElBQUksSUFBSSxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDdEcsT0FBTyxFQUFFLENBQUM7Z0JBR1gsSUFBSSxHQUFHLENBQUMsSUFBSSxLQUFLLHNCQUFzQixJQUFJLEdBQUcsQ0FBQyxPQUFPLElBQUksSUFBSSxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNuSCxPQUFPLEVBQUUsQ0FBQztnQkFHWCxNQUFNLEdBQUcsQ0FBQzthQUNWO1FBQ0YsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0NBQ0Q7QUF2REQsMENBdURDIn0=