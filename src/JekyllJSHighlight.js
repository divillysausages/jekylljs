"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.JekyllJSHighlight = void 0;
const highlight_js_1 = __importDefault(require("highlight.js"));
class JekyllJSHighlight {
    constructor() {
        this.config = {
            parentClassName: 'highlight',
            shouldWrap: true,
        };
    }
    highlight(code, lang) {
        var cn = this.config.parentClassName;
        var sw = this.config.shouldWrap;
        var clazz = (lang == "as3") ? "actionscript" : lang;
        if (clazz != null) {
            code = highlight_js_1.default.highlight(code, {
                language: clazz,
                ignoreIllegals: true
            }).value;
            clazz = "language-" + clazz;
        }
        else {
            code = highlight_js_1.default.highlightAuto(code).value;
            clazz = "";
        }
        var codeClazz = clazz;
        if (!sw)
            codeClazz = (codeClazz.length > 0) ? codeClazz + " " + cn : cn;
        var html = (sw) ? "<div class=\"" + cn + "\">" : "";
        html += "<pre><code class=\"" + codeClazz + "\">" + code + "</code></pre>";
        if (sw)
            html += "</div>";
        return html;
    }
}
exports.JekyllJSHighlight = JekyllJSHighlight;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSmVreWxsSlNIaWdobGlnaHQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJKZWt5bGxKU0hpZ2hsaWdodC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxnRUFBZ0M7QUFNaEMsTUFBYSxpQkFBaUI7SUFBOUI7UUFPUSxXQUFNLEdBQW9CO1lBQ2hDLGVBQWUsRUFBRSxXQUFXO1lBQzVCLFVBQVUsRUFBRSxJQUFJO1NBQ2hCLENBQUM7SUFpREgsQ0FBQztJQXZDTyxTQUFTLENBQUMsSUFBWSxFQUFFLElBQVk7UUFLMUMsSUFBSSxFQUFFLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUM7UUFDN0MsSUFBSSxFQUFFLEdBQVksSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUM7UUFHekMsSUFBSSxLQUFLLEdBQVcsQ0FBQyxJQUFJLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBSTVELElBQUksS0FBSyxJQUFJLElBQUksRUFBRTtZQUNsQixJQUFJLEdBQUcsc0JBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFO2dCQUMzQixRQUFRLEVBQUUsS0FBSztnQkFDZixjQUFjLEVBQUUsSUFBSTthQUNwQixDQUFDLENBQUMsS0FBSyxDQUFDO1lBQ1QsS0FBSyxHQUFHLFdBQVcsR0FBRyxLQUFLLENBQUM7U0FDNUI7YUFDSTtZQUNKLElBQUksR0FBRyxzQkFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDdEMsS0FBSyxHQUFHLEVBQUUsQ0FBQztTQUNYO1FBSUQsSUFBSSxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxFQUFFO1lBQ04sU0FBUyxHQUFHLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxHQUFHLEdBQUcsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUdoRSxJQUFJLElBQUksR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLEdBQUcsRUFBRSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ3BELElBQUksSUFBSSxxQkFBcUIsR0FBRyxTQUFTLEdBQUcsS0FBSyxHQUFHLElBQUksR0FBRyxlQUFlLENBQUM7UUFDM0UsSUFBSSxFQUFFO1lBQ0wsSUFBSSxJQUFJLFFBQVEsQ0FBQztRQUNsQixPQUFPLElBQUksQ0FBQztJQUNiLENBQUM7Q0FFRDtBQTNERCw4Q0EyREMifQ==