
import Liquid = require('liquid');

const Syntax = /((?:{{2}\s?)?[^%}]+(?:\s?}{2})?)/i
const SyntaxHelp = "Syntax Error in 'include_if_exists' - Valid syntax: {% include_if_exists {{ path }} %} or {% include_if_exists path %}"

export class IncludeIfExists extends Liquid.Tag {
	[x: string]: any; // used to dodge an error with "template" not existing
	public filepath = '';

	constructor(template, tagName, markup) {
		super(template, tagName, markup)

		const match = Syntax.exec(markup)
		if (!match) {
			throw new Liquid.SyntaxError(SyntaxHelp)
		}

		this.filepath = match[1].trim()
	}

	async subTemplate(context) {
		// if our filepath contains a tag, then we need to render it first
		if (this.filepath.startsWith('{{') && this.filepath.endsWith('}}')) {
			const pathTemplate = await this.template.engine.parse(this.filepath);
			this.filepath = await pathTemplate.render(context)
		}

		// remove any ending file type or the include will break
		const indexLastDot = this.filepath.lastIndexOf('.')
		const indexLastSlash = this.filepath.lastIndexOf('/')
		if (indexLastDot != -1 && indexLastDot > indexLastSlash) {
			this.filepath = this.filepath.substring(0, indexLastDot)
		}

		const src = await this.template
			.engine
			.fileSystem
			.readTemplateFile(this.filepath)
		return this.template.engine.parse(src)
	}

	async render(context) {
		return Promise.resolve()
			.then(() => this.subTemplate(context))
			.then(template => template.render(context))
			.catch(err => {
				if (err != null) {
					// catch if the file didn't exist, in which case, do nothing
					if (err.name === 'Liquid.FileSystemError' && err.message != null && err.message.indexOf('ENOENT') != -1)
						return '';

					// we might have an illegal filename, in which case, do nothing
					if (err.name === 'Liquid.ArgumentError' && err.message != null && err.message.indexOf('Illegal template name') != -1)
						return '';

					// it's something else, rethrow the error
					throw err;
				}
			});
	}
}