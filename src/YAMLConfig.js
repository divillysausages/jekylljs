"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.YAMLConfig = void 0;
class YAMLConfig {
    constructor() {
        this.source = ".";
        this.destination = "./_site";
        this.layouts_dir = "./_layouts";
        this.includes_dir = "./_includes";
        this.include = [".htaccess"];
        this.exclude = [];
        this.title = '';
        this.email = '';
        this.baseurl = "";
        this.url = '';
    }
    fromObj(obj) {
        for (var key in obj)
            this[key] = obj[key];
    }
}
exports.YAMLConfig = YAMLConfig;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiWUFNTENvbmZpZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIllBTUxDb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBR0EsTUFBYSxVQUFVO0lBQXZCO1FBT1EsV0FBTSxHQUFXLEdBQUcsQ0FBQztRQUtyQixnQkFBVyxHQUFXLFNBQVMsQ0FBQztRQUtoQyxnQkFBVyxHQUFXLFlBQVksQ0FBQztRQUtuQyxpQkFBWSxHQUFXLGFBQWEsQ0FBQztRQUtyQyxZQUFPLEdBQWEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUtsQyxZQUFPLEdBQWEsRUFBRSxDQUFDO1FBS3ZCLFVBQUssR0FBVyxFQUFFLENBQUM7UUFLbkIsVUFBSyxHQUFXLEVBQUUsQ0FBQztRQUtuQixZQUFPLEdBQVcsRUFBRSxDQUFDO1FBS3JCLFFBQUcsR0FBVyxFQUFFLENBQUM7SUFZekIsQ0FBQztJQUxPLE9BQU8sQ0FBQyxHQUFXO1FBQ3pCLEtBQUssSUFBSSxHQUFHLElBQUksR0FBRztZQUNsQixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7Q0FFRDtBQWhFRCxnQ0FnRUMifQ==