import { ConfigMeta, ConfigOpenGraph } from './ConfigHelper';
import { Content } from './Content';
import { YAMLConfig } from './YAMLConfig';

export type SiteMeta = ConfigMeta;
export type SiteOpenGraph = ConfigOpenGraph;
export type SiteTags = {
	size: number,
	[key: string]: Content[] | number, // "| number" to support the "size" property
};

/**
 * Class describing the site object, which is passed to the posts/pages when they're being
 * generated
 */
export class Site {

	/************************************************************/

	/**
	 * The default site title
	 */
	public title: string = '';

	/**
	 * The email of the author of the site
	 */
	public email: string = '';

	/**
	 * The baseurl for the site. Useful if we're under a sub-domain
	 */
	public baseurl: string = "";

	/**
	 * The URL for the site
	 */
	public url: string = '';

	/**
	 * All the posts for the site
	 */
	public posts: Content[] = [];

	/**
	 * All the pages for the site
	 */
	public pages: Content[] = [];

	/**
	 * All the tags for the site
	 */
	public tags: SiteTags = {
		size: 0,
	};

	/**
	 * The site's default meta data
	 */
	public meta?: SiteMeta;

	/**
	 * The site's default opengraph data
	 */
	public opengraph?: SiteOpenGraph;

	/************************************************************/

	/**
	 * Updates our Site vars from the YAML config
	 * @param yaml The YAMLConfig object
	 */
	public updateFromYAML(yaml: YAMLConfig): void {
		for (var key in yaml) {
			if (key in this)
				this[key] = yaml[key];
		}
	}

}