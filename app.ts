#!/usr/bin/env node

import FS = require('fs');
import HTTP = require('http');
import Liquid = require('liquid');
import NodeStatic = require('node-static');
import Path = require('path');
import RMRF = require('rimraf');
import YAML = require('js-yaml');
import { ConfigHelper } from './src/ConfigHelper';
import { Content } from './src/Content';
import { JekyllJSHighlight } from './src/JekyllJSHighlight';
import { YAMLConfig } from './src/YAMLConfig';
import { Site } from './src/Site';
import LiquidHighlight = require('./src/LiquidHighlight');
import { IncludeIfExists } from './src/IncludeIfExists';
import { marked as Marked } from 'marked';

/************************************************************/

type Context = {
	site: Site,
	page?: Content,
	content?: string,
}

// declare our vars
const highlighter: JekyllJSHighlight = new JekyllJSHighlight();	// the object that we use to highlight our code
const liquidEngine: Liquid.Engine = new Liquid.Engine(); // the engine for parsing our liquid tags
const config: ConfigHelper = new ConfigHelper(); // the object for reading our config
const yamlConfig: YAMLConfig = new YAMLConfig(); // our parsed yaml config
const context: Context = {
	site: new Site(),
}; // the context for passing to liquid parsing
const layouts: Record<string, string> = {}; // all the layouts that we're handling

/************************************************************/

/**
 * Runs our tool, exporting our site
 */
function run(): void {
	const startTime = new Date();

	// get the config json path
	const configJSONPath = (process.argv.length > 2) ? process.argv[2] : '';
	if (!configJSONPath) {
		console.error(`No config json path was passed - aborting`)
		process.exit(1);
	}

	_initConfig(configJSONPath);
	_createLog();
	_readYAMLConfig();
	_initContext();
	_initJekyllJSHighlight();
	_createMarked();
	_initLiquidEngine();

	// check if we're serving the site or just generating it
	const isServing = (process.argv.length > 3 && process.argv[3] === "serve");

	// if we're serving the site, then change the config
	if (isServing) {
		context.site.url = "http://localhost:" + config.server.port;
		console.debug(`We're serving the site; changing the site url to ${context.site.url}`);
	}

	// load our pages etc
	console.debug("Reading content");
	_readLayouts();
	_readContents("_posts", context.site.posts);
	_readContents("pages", context.site.pages);

	// set the size of our tags array - as our tags page uses site.tags.size, but as it's an object
	// it doesn't actually have this variable name. This is a bit of a hack, but I tested it on
	// actual Jekyll, and if you have a tag named "size" in a post, it won't build
	const numTags = Object.keys(context.site.tags).length;
	context.site.tags.size = numTags;
	console.info(`${Object.keys(layouts).length} layouts, ${context.site.posts.length} posts, and ${context.site.pages.length} pages were read. A total of ${numTags} tags were found`);

	// convert our content
	console.debug("Parsing liquid tags in our content");
	_convertPostsAndPages().then(function () {

		// clear any previous output
		var destDir = Path.join(config.src.path, yamlConfig.destination);
		_rmrfDir(destDir);
		FS.mkdirSync(destDir);
		return destDir;

	}).then(function (destDir: string) {

		// save our pages/posts
		console.info("Saving site content");
		return _savePostsAndPages(destDir);

	}).then(function () {

		// copy the rest of the stuff
		console.info("Saving other site files");
		return _copyAllOtherFiles(config.src.path);

	}).then(function () {

		// we're finished - log the time it took
		const totalTimeMS: number = ((new Date()).getTime() - startTime.getTime());
		let totalTimeS: number = Math.floor(totalTimeMS / 1000);
		let msg: string = "JekyllJS build finished in";
		let mins: number = 0;
		if (totalTimeS >= 60) {
			mins = Math.floor(totalTimeS / 60);
			totalTimeS -= 60 * mins;
			msg += (mins == 1) ? " 1 min" : " " + mins + " mins";
		}
		if (totalTimeS > 0)
			msg += (totalTimeS == 1) ? " 1 second!" : " " + totalTimeS + " seconds!";
		else if (mins == 0)
			msg += " no time at all!"
		console.info(msg);

		// if we're also serving the site, start our server
		if (isServing) {
			console.info("Starting local server");
			_createServer();
			return;
		}

		// we're not serving the site, so we can just quit the process	
		process.exit();

	}).catch(function (e) {
		console.error("Aborting build because an error occurred", e);
		process.exit();
		return;
	})
}
run();

/************************************************************/

// creates the logger that we're going to use
function _createLog(): void {
	console.info(`JekyllJS starting up on ${new Date()}`);
	console.debug("Config", config);
}

// reads the tool config
function _initConfig(path: string): void {
	try {
		const json = FS.readFileSync(path, 'utf-8');
		const jsonObj = JSON.parse(json);

		// try to load our config
		config.parse(jsonObj, path);
	} catch (e) {
		console.error("An error occurred when loading our config", e);
		process.exit(1);
	}
}

// reads the yaml config of our site
function _readYAMLConfig(): void {
	const path = Path.join(config.src.path, "_config.yml");
	console.debug(`Reading yaml config from ${path}`);
	let yaml = {};
	if (FS.existsSync(path))
		yaml = YAML.load(FS.readFileSync(path, "utf-8"));
	else
		console.warn(`The YAML config file (${path}) doesn't exist`);

	// create our object
	yamlConfig.fromObj(yaml);
	console.debug("YAML config:", yamlConfig);
}

// creates our context object
function _initContext(): void {
	context.site.meta = config.meta;
	context.site.opengraph = config.opengraph;
	context.site.updateFromYAML(yamlConfig);
}

// inits our JekyllJSHighlight object
function _initJekyllJSHighlight(): void {
	highlighter.config = config.highlight;
}

// creates our marked object
function _createMarked(): void {
	// use highlight.js to highlight markdown code
	const renderer = new Marked.Renderer();
	renderer.code = function (code: string, lang: string) {
		return highlighter.highlight(code, lang) + '\n';
	}

	Marked.setOptions({
		renderer: renderer
	});
}

// inits our liquid engine, which will parse our liquid tags
function _initLiquidEngine(): void {
	// create our entity map and escape function for escaping html/xml
	// NOTE: while this isn't exactly the best XML escape (it ignores ' and /),
	// it seems to match the jekyll xml_escape
	const escapeEntityMap: { [char: string]: string } = {
		"&": "&amp;",
		"<": "&lt;",
		">": "&gt;",
		'"': "&quot;",
		// "'": "&#39;",
		// "/": "&#x2F;",
		"¢": "&cent;",
		"£": "&pound;",
		"¥": "&yen;",
		"€": "&euro;",
		"©": "&copy;",
		"®": "&reg;"
	}

	// create our regex
	let escapeREStr = "[";
	for (let char in escapeEntityMap)
		escapeREStr += char;
	escapeREStr += "]";
	const escapeRE: RegExp = new RegExp(escapeREStr, "g");

	// create our escape function
	function escapeReplace(matchedEntity: string) {
		return escapeEntityMap[matchedEntity];
	}

	// add in any custom filters
	liquidEngine.registerFilters({

		// converts a date to an XML schema date string
		"date_to_xmlschema": function (date: string | Date): string {
			var d: Date = (typeof date === 'string') ? new Date(date) : date;
			return d.toISOString();
		},

		// escapes HTML chars so they can be used in XML
		"xml_escape": function (input: string): string {
			return input.replace(escapeRE, escapeReplace);
		},

		// escapes chars for use in a url
		"cgi_escape": function (input: string): string {
			return encodeURIComponent(input).replace(/%20/g, "+");
		},

		// base64 a string
		"base64": function (input: string): string {
			return Buffer.from(input, 'utf-8').toString('base64');
		}
	});

	// add in our custom tag for the highlight and include_if_exists
	LiquidHighlight.highlighter = highlighter;
	liquidEngine.registerTag("highlight", LiquidHighlight);
	liquidEngine.registerTag("include_if_exists", IncludeIfExists);

	// add a filesystem so we can include files
	const includePath: string = Path.join(config.src.path, yamlConfig.includes_dir);
	const lfs: Liquid.LocalFileSystem = new Liquid.LocalFileSystem(includePath, "html");
	liquidEngine.registerFileSystem(lfs);
}

// reads the layouts
function _readLayouts(): void {
	// make sure the dir exists
	const path = Path.join(config.src.path, yamlConfig.layouts_dir);
	if (!FS.existsSync(path)) {
		console.warn(`Can't read any layouts as the dir '${path}' doesn't exist`);
		return;
	}

	// read the dir
	FS.readdirSync(path).forEach(function (filename: string) {

		// make sure it's a html file
		if (!_isSupportedContentType(filename)) {
			console.info(`Ignoring the file '${filename}' (${path}) in layouts as it's not a supported content type (HTML/Markdown)`);
			return;
		}

		const contentsRaw: string = FS.readFileSync(Path.join(path, filename), 'utf-8');
		const layoutName: string = filename.substring(0, filename.lastIndexOf("."));
		layouts[layoutName] = contentsRaw;
	});
}

// reads the contents of a folder
function _readContents(dir: string, ar: Content[]): void {
	// make sure our path exists before doing anything
	const path = Path.join(config.src.path, dir);
	if (!FS.existsSync(path)) {
		console.warn(`The dir '${path}' doesn't exist`);
		return;
	}

	// parse the dir
	FS.readdirSync(path).forEach(function (filename: string) {

		// make sure it's a html file
		if (!_isSupportedContentType(filename)) {
			console.info(`Ignoring the file '${filename}' (${path}) as it's not a supported content type (HTML/Markdown)`);
			return;
		}

		// create our content object
		const content = _readContent(path, filename);
		if (content) {
			// extract our tags and add it
			_extractTags(content);
			ar.push(content);
		}
	});

	ar.sort(_sortContent);
}

// reads a single file, returning a Content object
function _readContent(path: string, filename: string): Content | undefined {
	// make sure our path exists
	const filePath = Path.join(path, filename);
	if (!FS.existsSync(filePath)) {
		console.warn(`Can't read the content '${filePath}' as the file doesn't exist`);
		return undefined;
	}

	// read the file
	const contentsRaw: string = FS.readFileSync(filePath, 'utf-8');
	const content: Content = new Content(Path.relative(config.src.path, filePath));
	content.readFromFile(filename, contentsRaw);

	return content;
}

// extracts the tags for a post/pages
function _extractTags(content: Content): void {
	if (content.tags == null || content.tags.length == 0)
		return;

	const len: number = content.tags.length;
	for (var i = 0; i < len; i++) {
		const t: string = content.tags[i];
		if (t in context.site.tags) {
			(<Content[]>context.site.tags[t]).push(content);
			(<Content[]>context.site.tags[t]).sort(_sortContent);
		}
		else
			context.site.tags[t] = [content];
	}
}

// converts our posts and pages, as certain pages (e.g. archive) make use of them, and don't
// convert twice. So we convert here, and replace the content
function _convertPostsAndPages(): Promise<void> {
	// as this uses promises, we need to chain the whole thing
	let sequence = Promise.resolve();

	// go through all our posts
	context.site.posts.forEach(function (post: Content) {

		// add them to the sequence
		sequence = sequence.then(function () {
			return _convertContent(post);
		});
	});

	// add in all our pages
	context.site.pages.forEach(function (page: Content) {

		// add them to the sequence
		sequence = sequence.then(function () {
			return _convertContent(page);
		});
	});

	// return the promise, which will fulfill when all files are converted
	return sequence;
}

// converts a single content, returning a Promise
function _convertContent(content: Content): Promise<void> {
	context.page = content;
	context.content = content.content;
	return liquidEngine.parseAndRender(content.content, context).then(function (result) {

		// save the converted result back to the content (as it can be used later if it's included anywhere)
		console.debug(`Finished parsing liquid in ${content.filename}`);
		content.content = result;

		// if this is a markdown file, then convert it
		if (content.isMarkdown) {
			console.debug(`Converting markdown file '${content.filename}'`);
			content.content = Marked(content.content);
		}
	}).catch(function (e: Error) {
		console.error(`Couldn't parse liquid in ${content.filename}`, e);
		throw e;
	})
}

// clears a directory and everything in it
function _rmrfDir(dir: string): void {
	if (FS.existsSync(dir) && FS.statSync(dir).isDirectory()) {
		console.debug(`Clearing dir ${dir}`);
		RMRF.sync(dir);
	}
}


// saves our posts and pages
function _savePostsAndPages(destRoot: string): Promise<void> {
	// as this uses promises, we need to chain the whole thing
	let sequence = Promise.resolve();

	// go through all our posts
	context.site.posts.forEach(function (post: Content) {

		// add them to the sequence
		sequence = sequence.then(function () {
			_ensureDirs(post.url, destRoot);
			return _saveContent(post, post.path, Path.join(destRoot, post.url, "index.html"));
		});
	});

	// add in all our pages
	context.site.pages.forEach(function (page: Content) {

		// add them to the sequence
		sequence = sequence.then(function () {
			_ensureDirs(page.url, destRoot);
			return _saveContent(page, page.path, Path.join(destRoot, page.url, "index.html"));
		});
	});

	// return the promise, which will fulfill when all files are saved
	return sequence;
}

// saves some content with possible frontmatter layout (assumes directories have been created)
function _saveContent(content: Content, path: string, destPath: string): Promise<void> {
	// failsafe
	if (content == null)
		return Promise.reject(new Error(`Can't save some content in path ${destPath} as null was passed`));

	// check if we have front matter, as we'll probably have to convert something
	if (content.frontMatter) {
		// check if we have a layout
		const layout: string | undefined = (content.layout) ? layouts[content.layout] : undefined;
		if (layout) {
			context.page = content;
			context.content = content.content;
			return liquidEngine.parseAndRender(layout, context).then(function (result) {
				console.debug(`Saving file ${content.url}`);
				FS.writeFileSync(destPath, result, 'utf-8');
			}).catch(function (e: Error) {
				console.error(`Couldn't save content ${content.url}`, e);
				throw e;
			})
		}
		else {
			// we don't have a layout, so just parse the file and save it
			return _convertContent(content).then(function () {
				console.debug(`Saving file ${content.url}`);
				FS.writeFileSync(destPath, content.content, 'utf-8');
			}).catch(function (e: Error) {
				console.error(`Couldn't save content ${content.url}`, e);
				throw e;
			})
		}
	}
	else {
		console.debug(`Saving file ${content.url}`);
		_copyFile(path, destPath); // just copy the file
		return Promise.resolve();
	}
}

// copies all the other files necessary
function _copyAllOtherFiles(dir: string, sequence?: Promise<void>): Promise<void> {
	// create our sequence if we need to
	if (!sequence)
		sequence = Promise.resolve();

	FS.readdirSync(dir).forEach(function (filename: string) {

		// get the full path
		const path: string = Path.join(dir, filename);

		// ignore hidden files, or files beginning with "_"
		if (/^[_\.]/.test(filename)) {
			// but only if they're not in the config.yml include
			if (yamlConfig.include.indexOf(filename) == -1) {
				console.debug(`Ignoring ${path} as it's a hidden file, or a special directory`);
				return;
			}
			console.debug(`Including ${path} as it's in our YAML include array`);
		}

		// if it's in our exclude array, ignore it
		if (yamlConfig.exclude.indexOf(filename) != -1) {
			console.debug(`Ignoring ${Path.join(dir, filename)} as it's in our YAML exclude array`);
			return;
		}

		// if it's a directory, recurse
		if (FS.statSync(path).isDirectory()) {
			if (filename == "pages")
				return; // ignore the pages dir, as we're already treating it
			_copyAllOtherFiles(path, sequence);
		}
		else {
			// make sure the directories for the file exists
			const destRootDir: string = Path.join(config.src.path, yamlConfig.destination);
			const destDir: string = Path.join(destRootDir, Path.relative(config.src.path, dir));
			const destPath: string = Path.join(destDir, filename);
			_ensureDirs(Path.relative(destRootDir, destDir), destRootDir);

			// read the file
			const content = _readContent(dir, filename);
			if (content) {
				// special case for the index file - replace the url
				if (content.url == "/index.html")
					content.url = "/";

				// if it has frontmatter, add it to our pages list
				if (content.frontMatter)
					context.site.pages.push(content);

				// save the file (using our promise sequence)
				sequence = sequence!.then(function () {
					return _saveContent(content, path, destPath);
				});
			}
		}
	});

	return sequence;
}

// copies a file from one path to another
function _copyFile(inPath: string, outPath: string): void {
	FS.copyFileSync(inPath, outPath);
}

// makes sure that all the directories for a particular path exist
function _ensureDirs(path: string, root: string): void {
	let curr: string = `${root}${Path.sep}`;
	const parts: string[] = (path.indexOf("/") != -1) ? path.split("/") : path.split(Path.sep); // as path might be a url, we check for forward slashes
	const len: number = parts.length;
	for (var i = 0; i < len; i++) {
		// the last part might be index.html, so we ignore creating a directory for that
		if (i == len - 1 && parts[i].indexOf(".") != -1)
			return;

		curr += `${parts[i]}${Path.sep}`;
		curr = Path.normalize(curr);
		if (!FS.existsSync(curr))
			FS.mkdirSync(curr);
	}
}

// the function used for sorting an array of content objects
function _sortContent(a: Content, b: Content): number {
	return b.date.getTime() - a.date.getTime(); // most recent first
}

// returns if this is content that we support
function _isSupportedContentType(filename: string): boolean {
	const index = filename.lastIndexOf('.');
	if (index == -1)
		return false;

	const ext: string = filename.substring(index + 1);
	return (ext == 'html' || ext == 'htm' || ext == 'md' || ext == 'markdown');
}

// the function to start our static server to server our site
function _createServer(): void {
	const serverDir: string = Path.join(config.src.path, yamlConfig.destination);

	// create our file server config
	const file = new NodeStatic.Server(serverDir, {
		cache: 0,	// no cache for our files
		gzip: true	// gzip our assets
	});

	// check to see if our 404 page exists
	if (config.src.fourOhFour) {
		const path404: string = Path.join(serverDir, config.src.fourOhFour);
		const exists: boolean = FS.existsSync(path404);
		if (!exists) {
			console.error(`The 404 path specified (${path404}) doesn't exist`);
			config.src.fourOhFour = undefined;
		}
		else if (FS.statSync(path404).isDirectory()) {
			if (config.src.fourOhFour.charAt(config.src.fourOhFour.length - 1) == "/")
				config.src.fourOhFour += "index.html";
			else
				config.src.fourOhFour += "/index.html";
		}

		// make sure it starts in a slash
		if (config.src.fourOhFour && config.src.fourOhFour.charAt(0) != "/")
			config.src.fourOhFour = `/${config.src.fourOhFour}`;
	}

	// create our basic server
	HTTP.createServer(function (request, response) {
		request.addListener('end', function () {

			// get our file server to serve the file
			file.serve(request, response, function (err) {

				if (err) {
					console.error(`There was an error getting ${request.url} - ${err.message}`);

					// if we're trying to get a path, and we've a 404 defined, serve that instead
					if (config.src.fourOhFour && request.url && (request.url.indexOf(".html") != -1 || request.url.charAt(request.url.length - 1) == "/") && err.status == 404)
						file.serveFile(config.src.fourOhFour, 404, {}, request, response);
					else {
						response.writeHead(err.status, err.headers);
						response.end();
					}
				}
			});
		}).resume();
	}).listen(config.server.port);

	console.info(`Local server started at ${context.site.url}`);
}